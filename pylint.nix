with import <nixpkgs> {};

pythonPackages.buildPythonPackage rec {
  pname = "pylint";
  version = "1.9.2";

  src = pythonPackages.fetchPypi {
    inherit pname version;
    sha256 = "fff220bcb996b4f7e2b0f6812fd81507b72ca4d8c4d05daf2655c333800cb9b3";
  };

  checkInputs = [
    pythonPackages.pytest
    pythonPackages.pytestrunner
    pythonPackages.pyenchant
  ];

  propagatedBuildInputs = [
    pythonPackages.astroid
    pythonPackages.isort
    pythonPackages.mccabe
    pythonPackages.configparser
    pythonPackages.backports_functools_lru_cache
  ];

  postPatch = lib.optionalString stdenv.isDarwin ''
    # Remove broken darwin test
    rm -vf pylint/test/test_functional.py
  '';

  checkPhase = "";

  postInstall = ''
    mkdir -p $out/share/emacs/site-lisp
    cp "elisp/"*.el $out/share/emacs/site-lisp/
  '';

  meta = with lib; {
    homepage = https://github.com/PyCQA/pylint;
    description = "A bug and style checker for Python";
    platforms = platforms.all;
    license = licenses.gpl1Plus;
    maintainers = with maintainers; [ nand0p ];
  };
}

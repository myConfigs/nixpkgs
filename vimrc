" vim: set tabstop=2 shiftwidth=2 expandtab fdm=indent:

let mapleader=","
let maplocalleader="\\"

""""""" Markdown
" Markdown is now included in vim, but by default .md is read as Modula-2
" files.  This fixes that, because I don't ever edit Modula-2 files :)
autocmd BufNewFile,BufReadPost *.md,*.markdown set filetype=markdown
autocmd FileType markdown set tw=80

""" Python
" Plug 'python-mode/python-mode', { 'branch': 'develop' }
  let g:pymode_rope_completion = 1
  let g:pymode_rope_complete_on_dot = 1
  let g:pymode_rope_autoimport = 1
  let g:pymode_rope_autoimport_import_after_complete = 1
  "let g:pymode_python = 'python3'

""""" End Filetypes ====================

""""" Utilities ========================
" Plug 'SirVer/ultisnips'
  let g:UltiSnipsExpandTrigger="<tab>" " Trigger configuration. Do not use <tab> if you use https://github.com/Valloric/YouCompleteMe.
  let g:UltiSnipsJumpForwardTrigger="<c-b>"
  let g:UltiSnipsJumpBackwardTrigger="<c-z>"
  let g:UltiSnipsEditSplit="vertical" " If you want :UltiSnipsEdit to split your window.
"Plug 'xolox/vim-easytags'
  let g:easytags_async = 1

" Plug 'scrooloose/nerdcommenter'  " Line commenting
  let g:NERDSpaceDelims = 1 " Add spaces after comment delimiters by default
  let g:NERDCompactSexyComs = 1 " Use compact syntax for prettified multi-line comments
  let g:NERDDefaultAlign = 'left' " Align line-wise comment delimiters flush left instead of following code indentation
  let g:NERDAltDelims_java = 1 " Set a language to use its alternate delimiters by default
  let g:NERDCustomDelimiters = { 'c': { 'left': '/**','right': '*/' } } " Add your own custom formats or override the defaults
  let g:NERDCommentEmptyLines = 1 " Allow commenting and inverting empty lines (useful when commenting a region)
  let g:NERDTrimTrailingWhitespace = 1 " Enable trimming of trailing whitespace when uncommenting
  let g:NERDToggleCheckAllLines = 1 " Enable NERDCommenterToggle to check all selected lines is commented or not
  noremap <Leader>cc :NERDComComment<CR>
  noremap <Leader>cu :NERDComUncommentLine<CR>

" Plug 'mileszs/ack.vim'
let g:ack_autoclose = 1
"  use NERDTree and opening ack.vim results in a vertical split displacing it
let g:ack_mappings = {
  \  'v':  '<C-W><CR><C-W>L<C-W>p<C-W>J<C-W>p',
  \ 'gv': '<C-W><CR><C-W>L<C-W>p<C-W>J' }
if executable('ag')
  let g:ackprg = 'ag -u'
endif
noremap <Leader>fw :Ack<CR>

""""" End Utilities ====================

""""" UI Plugs =======================
" Plug 'scrooloose/nerdtree'
  map <leader>nt :NERDTreeToggle<CR>
" Plug 'ryanoasis/vim-devicons'
  set encoding=UTF-8
"
" Plug 'epeli/slimux'
  map <C-c><C-c> :SlimuxREPLSendLine<CR>
  vmap <C-c><C-c> :SlimuxREPLSendSelection<CR>
" Plug 'majutsushi/tagbar'
  autocmd BufEnter *.py TagbarOpen
  autocmd FileType nerdtree TagbarClose
  autocmd BufEnter Preview TagbarClose
  " autocmd BufLeave *.py TagbarClose
  " python-mode 'pydocs' and 'automcompletion' clash with tagbar when the
  " buffer enters or leaves the .py file, as set above.
  let g:tagbar_left = 1

  let g:tagbar_type_groovy = {
        \ 'ctagstype' : 'groovy',
        \ 'kinds'     : [
        \ 'p:package',
        \ 'c:class',
        \ 'i:interface',
        \ 'f:function',
        \ 'v:variables',
        \ ]
        \ }

  let g:tagbar_type_nix = {
        \ 'ctagstype' : 'nix',
        \ 'kinds'     : [
        \ 'f:function',
        \ ]
        \ }

" Plug 'vim-airline/vim-airline-themes'
  set laststatus=2               " enable airline even if no splits
  "let g:airline_theme='luna'
  let g:airline_theme='papercolor'
  let g:airline_powerline_fonts=1
  let g:airline_enable_branch=1
  let g:airline_enable_syntastic=0
  let g:airline#extensions#ale#enabled = 1
  let g:airline_powerline_fonts = 1
  let g:airline_left_sep = ''
  let g:airline_right_sep = ''
  let g:airline_linecolumn_prefix = '␊ '
  let g:airline_linecolumn_prefix = '␤ '
  let g:airline_linecolumn_prefix = '¶ '
  let g:airline_branch_prefix = '⎇ '
  let g:airline_paste_symbol = 'ρ'
  let g:airline_paste_symbol = 'Þ'
  let g:airline_paste_symbol = '∥'
  let g:airline#extensions#tabline#enabled = 0
  let g:airline_mode_map = {
        \ 'n' : 'N',
        \ 'i' : 'I',
        \ 'R' : 'REPLACE',
        \ 'v' : 'VISUAL',
        \ 'V' : 'V-LINE',
        \ 'c' : 'CMD   ',
        \ '': 'V-BLCK',
        \ }
""""" End UI Plugs ===================

""""" Code Navigation ===============
" Plug 'kien/ctrlp.vim'
  let g:ctrlp_match_window_bottom = 1    " Show at bottom of window
  let g:ctrlp_working_path_mode = 'ra'   " Our working path is our VCS project or the current directory
  let g:ctrlp_mru_files = 1              " Enable Most Recently Used files feature
  let g:ctrlp_jump_to_buffer = 2         " Jump to tab AND buffer if already open
  let g:ctrlp_open_new_file = 'v'        " open selections in a vertical split
  let g:ctrlp_open_multiple_files = 'vr' " opens multiple selections in vertical splits to the right
  let g:ctrlp_arg_map = 0
  let g:ctrlp_dotfiles = 0               " do not show (.) dotfiles in match list
  let g:ctrlp_showhidden = 0             " do not show hidden files in match list
  let g:ctrlp_split_window = 0
  let g:ctrlp_max_height = 40            " restrict match list to a maxheight of 40
  let g:ctrlp_use_caching = 0            " don't cache, we want new list immediately each time
  let g:ctrlp_max_files = 0              " no restriction on results/file list
  let g:ctrlp_working_path_mode = ''
  let g:ctrlp_dont_split = 'NERD_tree_2' " don't split these buffers
  set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*.exe,*.pyc
  let g:ctrlp_custom_ignore = {
        \ 'dir':  '\v[\/]\.(git|hg|svn|gitkeep)$',
        \ 'file': '\v\.(exe|so|dll|log|gif|jpg|jpeg|png|psd|DS_Store|ctags|gitattributes)$'
        \ }
  " let g:ctrlp_match_func = { 'match': 'pymatcher#PyMatch' }
  " let g:ctrlp_user_command = ['.git/', 'cd %s && git ls-files --exclude-standard -co'] " if you want to use git for this rather than ag
  let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'
  let g:ctrlp_prompt_mappings = {
        \ 'AcceptSelection("e")': ['<c-e>', '<c-space>'],
        \ 'AcceptSelection("h")': ['<c-x>', '<c-cr>', '<c-s>'],
        \ 'AcceptSelection("t")': ['<c-t>'],
        \ 'AcceptSelection("v")': ['<cr>'],
        \ 'PrtSelectMove("j")':   ['<c-j>', '<down>', '<s-tab>'],
        \ 'PrtSelectMove("k")':   ['<c-k>', '<up>', '<tab>'],
        \ 'PrtHistory(-1)':       ['<c-n>'],
        \ 'PrtHistory(1)':        ['<c-p>'],
        \ 'ToggleFocus()':        ['<c-tab>'],
        \}

""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Ctrl B for buffer related mappings
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""
noremap <Leader>bl :CtrlPBuffer<CR> " cycle between buffer
nnoremap <Leader>bb :bn<CR> "create (N)ew buffer
nnoremap <Leader>bd :bdelete<CR> "(D)elete the current buffer
nnoremap <Leader>bu :bunload<CR> "(U)nload the current buffer
nnoremap <Leader>bl :setnomodifiable<CR> " (L)ock the current buffer"

""""" End Code Navigation ===========


" Turn on plugins, indentation, etc.
filetype plugin indent on

"  You will load your plugin here
"  Make sure you use single quotes
" Initialize plugin system
" call plug#end()


""" UI Tweaks ==========================
set number " line numbering
set relativenumber
set t_Co=256 " Force 256 colors
" make splits like preview windows to show at the bottom instead of the top
set splitbelow

" add symbols for 'tabs'
set list
set listchars=tab:\⇥\ ,trail:·,extends:>,precedes:<,nbsp:+

" Turn off menu in gui
set guioptions="agimrLt"
" Turn off mouse click in gui
set mouse=""

" in case t_Co alone doesn't work, add this as well:
" i.e. Force 256 colors harder
let &t_AB="\e[48;5;%dm"
let &t_AF="\e[38;5;%dm"

set t_ut= " improve screen clearing by using the background color
set background=light
syntax enable
"colorscheme moonshine
colorscheme PaperColor
set enc=utf-8
set term=screen-256color
let $TERM='screen-256color'

set colorcolumn=90 " Show a column on 90 char right

set wildmode=list

set showmatch " show matching paranteses

" Highlighting line or number follows vvvvv
"
set cul " highlight current line
" If you want to just highlight the line number:
" hi clear CursorLine
" augroup CLClear
"   autocmd! ColorScheme * hi clear CursorLine
" augroup END
" hi CursorLineNR cterm=bold
" augroup CLNRSet
"     autocmd! ColorScheme * hi CursorLineNR cterm=bold
" augroup END


" change vim cursor depending on the mode
if has("unix")
	let s:uname = system("uname -s")
	if s:uname == "Darwin\n"
		" OS X iTerm 2 settings
		if exists('$TMUX')
			let &t_SI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=1\x7\<Esc>\\"
			let &t_EI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=0\x7\<Esc>\\"
		else
			let &t_SI = "\<Esc>]50;CursorShape=1\x7"
			let &t_EI = "\<Esc>]50;CursorShape=0\x7"
		endif
	else
		" linux settings (gnome-terminal)
		" TODO: Presently in GNOME3 terminal seems to ignore this gconf setting.
		" Need to open a bug with them...
		if has("autocmd")
			au InsertEnter * silent execute "!gconftool-2 --type string --set /apps/gnome-terminal/profiles/Default/cursor_shape ibeam"
			au InsertLeave * silent execute "!gconftool-2 --type string --set /apps/gnome-terminal/profiles/Default/cursor_shape block"
			au VimLeave * silent execute "!gconftool-2 --type string --set /apps/gnome-terminal/profiles/Default/cursor_shape ibeam"
		endif
	endif
endif

" Show trailing whitespace and spaces before a tab:
:highlight ExtraWhitespace ctermbg=red guibg=red
:autocmd Syntax * syn match ExtraWhitespace /\s\+$\| \+\ze\\t/
""" End UI Tweaks ======================


""" Keyboard shortcut setup =====================
let mapleader=","
let maplocalleader="\\"

nnoremap <Leader><Leader> :e#<CR> " open the previous file

set hlsearch " hightlight searchs
" Remove highlights
" Clear the search buffer when hitting return
nnoremap <cr> :nohlsearch<cr>

" NO ARROW KEYS COME ON
map <Left> :echo "no!"<cr>
map <Right> :echo "no!"<cr>
map <Up> :echo "no!"<cr>
map <Down> :echo "no!"<cr>

" Custom split opening / closing behaviour
map <C-N> :vsp .<CR>
map <C-C> :q<CR>

" reselect pasted content:
noremap gV `[v`]

" Redraw my screen
nnoremap U :syntax sync fromstart<cr>:redraw!<cr>

" Keep the cursor in place while joining lines
nnoremap J mzJ`z

" Split line (sister to [J]oin lines above)
" The normal use of S is covered by cc, so don't worry about shadowing it.
nnoremap S i<cr><esc>^mwgk:silent! s/\v +$//<cr>:noh<cr>`w

" force writes as sudo
cnoremap w!! execute 'silent! write !sudo tee % >/dev/null' <bar> edit!

""" End Keyboard shortcut setup =================

""" Vim environment handling tweaks ====
""""" BACKUP / TMP FILES

" Start scrolling 3 lines before the border
set scrolloff=3

" Makes foo-bar considered one word
set iskeyword+=-
""" End Vim environment handling tweaks

""" File navigation ====================
" case insensitive highlight matches in normal/visual mode
nnoremap / /\v
vnoremap / /\v

""fix backspace
set backspace=indent,eol,start
""" End File navigation ================

""" Auto Commands ======================
""""" Filetypes ========================
augroup erlang
	au!
	au BufNewFile,BufRead *.erl setlocal tabstop=4
	au BufNewFile,BufRead *.erl setlocal shiftwidth=4
	au BufNewFile,BufRead *.erl setlocal softtabstop=4
	au BufNewFile,BufRead relx.config setlocal filetype=erlang
augroup END
""""" End Filetypes ====================

""""" Normalization ====================
" Delete trailing white space on save
func! DeleteTrailingWS()
	exe "normal mz"
	%s/\s\+$//ge
	exe "normal `z"
endfunc
au BufWrite * silent call DeleteTrailingWS()
""""" End Normalization ================

""" End Auto Commands ==================

""""" Backups  ====================
if !isdirectory($HOME."~/.vim/tmp")
	call mkdir($HOME."/.vim/tmp", "p")
endif
set backupdir=~/.vim/tmp/                   " for the backup files
set directory=~/.vim/tmp/                   " for the swap files
""""" End Backups  ================

"" Set history
set hidden
set history=1000
""""" End History ================


"""" Project specific
set exrc " load a local .vimrc in the project"
set secure " but disable autocmd and write from the local project .vimrc file"

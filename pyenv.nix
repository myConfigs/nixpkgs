with import <nixpkgs> {};

stdenv.mkDerivation rec {
  name = "pyenv-${version}";
  version = "1.2.5";
  rev = "v${version}";

  buildInputs = [ git zlib python27Packages.pip ];

  src = fetchFromGitHub {
    inherit rev;
    owner = "pyenv";
    repo = "pyenv";
    sha256 = "1n72d477g15qv6ac7v6az98pgl9z4blb1llpicj5djfj02syw90n";
  };

  phases = "installPhase";

  installPhase = ''
   mkdir -p $out
   cp -r ${src}/* $out/
  '';

  inherit zlib;
}


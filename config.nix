{
  allowUnfree = true;
  substituters = https://cache.nixos.org https://azulinho.cachix.org;
  trusted-public-keys = cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY= azulinho.cachix.org-1:QBxziw/PMAeZkjh3vVT36QwDnMBVneuk864qnAmy8kY=;
  # http://nicknovitski.com/vim-nix-syntax
  vim.ftNix = false;
  max-jobs = 8;
  build-cores=8;
}


{ pkgs, lib, services,  ... }:

let

  # override pylint to work with py 2.7
  pylint =  import ./pylint.nix;

  # helper function to work around VIM plugins and nix
  # https://github.com/NixOS/nixpkgs/issues/39364
  loadPlugin = plugin: ''
      set rtp^=${plugin.rtp}
      set rtp+=${plugin.rtp}/after
    '';

    vim-one = pkgs.vimUtils.buildVimPlugin {
      name = "vim-one";
      src = pkgs.fetchFromGitHub {
        owner = "rakr";
        repo = "vim-one";
        rev = "08aca1b5d8cd83106a430f150aa77b4148f6cd5e";
        sha256 = "13ldimmm839s8a96l37s8pjx1rrr0cnkjyfiapp1c1n640qn1rd9";
      };
      buildInputs = [ pkgs.zip pkgs.vim ];
    };

    papercolor-theme = pkgs.vimUtils.buildVimPlugin {
      name = "papercolor-theme";
      src = pkgs.fetchFromGitHub {
        owner = "NLKNguyen";
        repo = "papercolor-theme";
        rev = "5bd7d5b3f9dd0650e6bbd1756feebe1651fa6ba8";
        sha256 = "0fskm9gz81dk8arcidrm71mv72a7isng1clssqkqn5wnygbiimsn";
      };
      buildInputs = [ pkgs.zip pkgs.vim ];
    };

    delimate = pkgs.vimUtils.buildVimPlugin {
      name = "delimate";
      src = pkgs.fetchFromGitHub {
        owner = "Raimondi";
        repo = "delimitMate";
        rev = "728b57a6564c1d2bdfb9b9e0f2f8c5ba3d7e0c5c";
        sha256 = "0fskm9gz81dk8arcidrm71mv72a7isng1clssqkqn5wnygbiimsn";
      };
      buildInputs = [ pkgs.zip pkgs.vim ];
    };

    slimux = pkgs.vimUtils.buildVimPlugin {
      name = "slimux";
      src = pkgs.fetchFromGitHub {
        owner = "epeli";
        repo = "slimux";
        rev = "226cd18a1bb4dee9b576083dc93d5df7f1aedbf1";
        sha256 = "196j1cd6dxjvn8hwwff3yp3c3g2zzb09iz7pdpqiz8kp4nzr3pcf";
      };
    };

    gv = pkgs.vimUtils.buildVimPlugin {
      name = "gv";
      src = pkgs.fetchFromGitHub {
        owner = "junegunn";
        repo = "gv.vim";
        rev = "d6f68abc902c8405a997b72063aa3a350e073c52";
        sha256 = "1j41llan6di7lmnpamf5x5dsbvnm142kbzhly4mjphpq5k7lm9sn";
      };
    };

    minibufexpl = pkgs.vimUtils.buildVimPlugin {
      name = "minibufexpl";
      src = pkgs.fetchFromGitHub {
        owner = "fholgado";
        repo = "minibufexpl.vim";
        rev = "ad72976ca3df4585d49aa296799f14f3b34cf953";
        sha256 = "1bfq8mnjyw43dzav8v1wcm4rrr2ms38vq8pa290ig06247w7s7ng";
      };
    };
  my_vim_configurable = pkgs.vim_configurable.override {
    python = pkgs.python36Full.withPackages (ps: with ps; [ pylama jedi pylint flake8 ]);
    wrapPythonDrv = true;
  };

  my_vimPlugins = with pkgs.vimPlugins;  [
    ack-vim
    awesome-vim-colorschemes
    base16-vim
    ctrlp
    ctrlp-cmatcher
    ctrlp-py-matcher
    ctrlp-z
    delimate # automate ( )
    editorconfig-vim
    gv # git viewer
    minibufexpl
    nerdcommenter
    nerdtree
    nerdtree-git-plugin
    papercolor-theme
    python-mode
    slimux
    supertab
    syntastic
    tagbar
    ultisnips
    vim-airline
    vim-airline-themes
    vim-colorschemes
    vim-colors-solarized
    vim-devicons
    vim-easytags
    vim-elixir
    vim-fugitive
    vim-javascript
    vim-jsonnet
    vim-misc
    vim-nix
    vim-one
    vim-polyglot
    vim-ruby
    vim-signify # shows git changes in the modified file
    vim-snippets
    vinegar # navigate up a directory with "-" in netrw
  ];


in

{
  home.packages = [
    ( import ./pyenv.nix )
    pylint

    (my_vim_configurable.customize {
      name = "vim";
      vimrcConfig.customRC = ''
        " Workaround for broken handling of packpath by vim8/neovim for ftplugins
        filetype off | syn off
        ${builtins.concatStringsSep "\n"
          (map loadPlugin my_vimPlugins )}
        filetype indent plugin on | syn on

        ${ (builtins.readFile ./vimrc) }

      '';

      vimrcConfig.packages.myVimPackages = {
        start = my_vimPlugins;
      };
    })

    pkgs.adobe-reader
    pkgs.afuse
    pkgs.ag
    pkgs.anonymousPro
    pkgs.ansible
    pkgs.antiword
    pkgs.arc-icon-theme
    pkgs.arc-theme
    pkgs.aria2
    pkgs.asciidoc-full-with-plugins
    pkgs.aspell
    pkgs.aspell
    pkgs.aspellDicts.es
    pkgs.aspellDicts.pt_PT
    pkgs.aspellDicts.uk
    pkgs.awscli
    pkgs.awsebcli
    pkgs.aws-iam-authenticator
    pkgs.awslogs
    pkgs.aws-rotate-key
    pkgs.aws-vault
    pkgs.backblaze-b2
    pkgs.bashCompletion
    pkgs.blueman
    pkgs.bluez
    pkgs.bluez-tools
    pkgs.borgbackup
    pkgs.breeze-icons
    pkgs.bzip2
    pkgs.cachix
    pkgs.cantarell-fonts
    pkgs.chruby
    pkgs.chruby
    pkgs.cifs-utils
    pkgs.clamav
    pkgs.docker_compose
    pkgs.docker-machine-kvm
    pkgs.dosfstools
    pkgs.elementary-gtk-theme
    pkgs.elementary-icon-theme
    pkgs.elementary-xfce-icon-theme
    pkgs.epdfview
    pkgs.exfat
    pkgs.exfat-utils
    pkgs.fantasque-sans-mono
    pkgs.ffmpeg-full
    pkgs.franz
    pkgs.fzf
    pkgs.gitAndTools.git-annex
    pkgs.gitAndTools.git-annex-remote-b2
    pkgs.gitAndTools.git-annex-remote-rclone
    pkgs.gitAndTools.gitflow
    pkgs.gitAndTools.gitFull
    pkgs.gitAndTools.hub
    pkgs.gitAndTools.tig
    pkgs.git-crypt
    pkgs.gnome3.adwaita-icon-theme
    pkgs.gnome3.gnome-calculator
    pkgs.gnome3.gnome-keyring
    pkgs.gnome-themes-extra
    pkgs.gnome-themes-standard
    pkgs.gnupg
    pkgs.go
    pkgs.google-chrome
    pkgs.hicolor-icon-theme
    pkgs.hplipWithPlugin
    pkgs.htop
    pkgs.htop
    pkgs.jdk8
    pkgs.jq
    pkgs.kubernetes-helm
    pkgs.lastfmsubmitd
    pkgs.lastpass-cli
    pkgs.liberation_ttf
    pkgs.libxml2
    pkgs.lxappearance-gtk3
    pkgs.maia-icon-theme
    pkgs.mate.mate-icon-theme
    pkgs.mate.mate-icon-theme-faenza
    pkgs.mplayer
    pkgs.ncat
    pkgs.nmap
    pkgs.nodejs
    pkgs.noto-fonts-cjk
    pkgs.noto-fonts-emoji
    pkgs.noto-fonts-extra
    pkgs.numix-cursor-theme
    pkgs.numix-gtk-theme
    pkgs.numix-icon-theme
    pkgs.numix-icon-theme-circle
    pkgs.numix-icon-theme-square
    pkgs.numix-solarized-gtk-theme
    pkgs.openshift
    pkgs.pipenv
    pkgs.playonlinux
    pkgs.powerline-fonts
    pkgs.powershell
    pkgs.powertop
    pkgs.pypi2nix
    pkgs.python27Full
    pkgs.python27Packages.docker_compose
    pkgs.python27Packages.ipython
    pkgs.python27Packages.pip
    pkgs.python27Packages.powerline
    pkgs.python27Packages.virtualenv
    pkgs.python27Packages.websocket_client
    pkgs.python27Packages.youtube-dl
    pkgs.python36Full
    pkgs.python36Packages.ipython
    pkgs.python36Packages.pip
    pkgs.python36Packages.powerline
    pkgs.python36Packages.virtualenv
    pkgs.ranger
    pkgs.rclone
    pkgs.recordmydesktop
    pkgs.redshift
    pkgs.rofi
    pkgs.rofi-menugen
    pkgs.rofi-pass
    pkgs.rsnapshot
    pkgs.rsync
    pkgs.ruby
    pkgs.ruby
    pkgs.s3cmd
    pkgs.scaleway-cli
    pkgs.skippy-xd
    pkgs.skype
    pkgs.slack
    pkgs.source-code-pro
    pkgs.sshfs
    pkgs.symbola
    pkgs.synapse
    pkgs.sysstat
    pkgs.system-config-printer
    pkgs.tango-icon-theme
    pkgs.tcpdump
    pkgs.telnet
    pkgs.terminus_font
    pkgs.terminus_font_ttf
    pkgs.theme-vertex
    pkgs.thinkfan
    pkgs.tightvnc
    pkgs.tilda
    pkgs.tinc
    pkgs.tmate
    pkgs.tmux
    pkgs.tmuxp
    pkgs.transmission_gtk
    pkgs.travis
    pkgs.ttf_bitstream_vera
    pkgs.ubuntu_font_family
    pkgs.universal-ctags
    pkgs.vagrant
    pkgs.vlc
    pkgs.vscode
    pkgs.weechat
    pkgs.wget
    pkgs.xmr-stak
    pkgs.zlib # for pyenv
    #tmux21
  ];

  programs = {
    home-manager = {
      enable = true;
      path = https://github.com/rycee/home-manager/archive/release-18.03.tar.gz;
    };

    command-not-found.enable = true;

    git = {
      enable = false;
      userName = "Azul";
      userEmail = "mail@azulinho.com";
      signing = {
        key = "mail@azulinho.com";
        signByDefault = true;
      };

      extraConfig = ''
        [gc]
          autoDetach = false
        [diff]
          tool = vimdiff
        [difftool]
          prompt = false
        [alias]
          d = difftool
      '';
    };
  };
  services.parcellite.enable = true;
}
